import { Route, Switch } from "react-router-dom";
import Home from "../pages/home";
import Alive from "../pages/alive";
import Dead from "../pages/dead";

const Router = ({ charactersList }) => {
  return (
    <Switch>
      <Route exact path="/">
        <Home charactersList={charactersList} />
      </Route>
      <Route path="/alive">
        <Alive charactersList={charactersList} />
      </Route>
      <Route path="/dead">
        <Dead charactersList={charactersList} />
      </Route>
    </Switch>
  );
};

export default Router;
