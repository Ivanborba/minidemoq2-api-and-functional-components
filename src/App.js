import "./App.css";
import { useState, useEffect } from "react";
import axios from "axios";
import Router from "./router";
import Header from "./components/header";

function App() {
  const [characters, setCharacters] = useState([]);
  const [url, setUrl] = useState("https://rickandmortyapi.com/api/character");

  const request = () => {
    axios.get(url).then((res) => {
      setCharacters([...characters, ...res.data.results]);
      setUrl(res.data.info.next);
    });
  };

  useEffect(() => {
    request();
  }, [url]);

  return (
    <div className="App">
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Header />
        <Router charactersList={characters} />
      </div>
      -
    </div>
  );
}

export default App;
