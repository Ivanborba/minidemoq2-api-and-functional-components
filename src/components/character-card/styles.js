import { Paper } from "@material-ui/core";
import styled, { css } from "styled-components";

export const StyledPaper = styled(Paper)`
  height: 180px;
  width: 150px;
  margin-top: 10px;
  padding: 15px;
  p {
    color: blue;
  }

  ${({ color }) =>
    css`
      p {
        color: ${color};
      }
    `}
`;
