import { StyledPaper } from "./styles";

const CharacterCard = ({ character, color }) => {
  return (
    <StyledPaper elevation={5} color={color}>
      <img
        alt={character.name}
        src={character.image}
        style={{ width: "100px" }}
      />
      <p>{character.name}</p>
      <p>{character.status}</p>
    </StyledPaper>
  );
};

export default CharacterCard;
