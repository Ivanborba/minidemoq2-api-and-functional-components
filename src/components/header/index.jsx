import { useHistory } from "react-router-dom";

const Header = () => {
  const history = useHistory();
  return (
    <div>
      <button onClick={() => history.push("/")}>Todos</button>
      <button onClick={() => history.push("/alive")}>Vivos</button>
      <button onClick={() => history.push("/dead")}>Mortos</button>
    </div>
  );
};

export default Header;
