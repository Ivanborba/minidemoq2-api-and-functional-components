import CharacterCard from "../../components/character-card";

const Alive = ({ charactersList }) => {
  return (
    <div>
      {charactersList
        .filter((actual) => actual.status === "Alive")
        .map((actual, index) => {
          return (
            <CharacterCard
              style={{ border: "1px solid green" }}
              character={actual}
              key={index}
              color={"green"}
            >
              {actual.name}
            </CharacterCard>
          );
        })}
    </div>
  );
};

export default Alive;
