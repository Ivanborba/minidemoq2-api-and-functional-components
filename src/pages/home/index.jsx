import CharacterCard from "../../components/character-card";

const Home = ({ charactersList }) => {
  return (
    <div>
      {charactersList.map((actual, index) => {
        return (
          <CharacterCard character={actual} key={index}>
            {actual.name}
          </CharacterCard>
        );
      })}
    </div>
  );
};

export default Home;
