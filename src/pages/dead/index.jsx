import CharacterCard from "../../components/character-card";

const Dead = ({ charactersList }) => {
  return (
    <div>
      {charactersList
        .filter((actual) => actual.status === "Dead")
        .map((actual, index) => {
          return (
            <CharacterCard
              style={{ border: "1px solid red" }}
              character={actual}
              key={index}
              color={"red"}
            >
              {actual.name}
            </CharacterCard>
          );
        })}
    </div>
  );
};

export default Dead;
